cmake_policy(PUSH)

cmake_minimum_required(VERSION 2.8.12)

if(LCARD_SDK_MAKE_INSTALLER)
    set(LPCIE_SDK_CONV_DIR ${CMAKE_CURRENT_BINARY_DIR}/sdk-conv)
    file(MAKE_DIRECTORY ${LPCIE_SDK_CONV_DIR})


    string(REGEX REPLACE "/" "\\\\" CONVERT_SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR})
    string(REGEX REPLACE "/" "\\\\" CONVERT_DEST_DIR ${LPCIE_SDK_CONV_DIR})
    string(REGEX REPLACE "/" "\\\\" CMAKE_CURRENT_SOURCE_DIR_WIN ${LPCIE_SDK_SOURCE_DIR})

    string(REGEX REPLACE "/" "\\\\" LIBXML2_DLL ${LIBXML2_DLL})
    string(REGEX REPLACE "/" "\\\\" LIBXML2_ICONV_DLL ${LIBXML2_ICONV_DLL})
    string(REGEX REPLACE "/" "\\\\" LIBXML2_ZLIB_DLL ${LIBXML2_ZLIB_DLL})
    string(REGEX REPLACE "/" "\\\\" GACUTIL_EXEC ${GACUTIL_EXEC})
    string(REGEX REPLACE "/" "\\\\" BONJOUR_INSTALLER_DIR ${BONJOUR_INSTALLER_DIR})


    configure_file(${LPCIE_SDK_SOURCE_DIR}/win_installer/conv_cp1251.bat.in
                   ${LPCIE_SDK_BINARY_DIR}/conv_sdk_cp1251.bat @ONLY)


    add_custom_target(sdk-conv
        COMMAND cmd /c conv_sdk_cp1251.bat
        WORKING_DIRECTORY ${LPCIE_SDK_BINARY_DIR}
        )


    set(INSTALLER_NAME lpcie-setup-${LPCIE_SDK_VERSION}.exe)

    #конфигурируем NSIS-файл
    configure_file(${LPCIE_SDK_SOURCE_DIR}/win_installer/lpcie_install.nsi.in
                   ${LPCIE_SDK_BINARY_DIR}/lpcie_install.nsi @ONLY)


    #сборка установщика (включая все входящие утилиты) */
    add_custom_target(win-installer       
        #по nsi файлу создаем установщик
        COMMAND ${NSIS_EXEC} ${LPCIE_SDK_BINARY_DIR}/lpcie_install.nsi

        #DEPENDS driver_pci
        DEPENDS x502api_32
        DEPENDS x502api_64
        DEPENDS lpcieNet
        DEPENDS x502api_pdf
        DEPENDS lxfw-update
        DEPENDS x502_low_level_pdf
        DEPENDS sdk-conv
        WORKING_DIRECTORY ${LPCIE_SDK_BINARY_DIR}
        )
    #цель для сборки установщика без зависимостей
    add_custom_target(win-installer-nodep
        COMMAND ${NSIS_EXEC} ${LPCIE_SDK_BINARY_DIR}/lpcie_install.nsi
        WORKING_DIRECTORY ${LPCIE_SDK_BINARY_DIR}
        )


    if(NOT WIN_INSTALLER_NOSIGN)
        #подписываем установщик при неободимости
        add_custom_command(TARGET win-installer POST_BUILD
            COMMAND "${SIGNTOOL_EXEC}" sign /v /n "${SIGN_COMPANY_NAME}" /t ${CERT_TIMESTAMP_URL} ${INSTALLER_NAME}
            WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
        )
    endif(NOT WIN_INSTALLER_NOSIGN)
endif(LCARD_SDK_MAKE_INSTALLER)




cmake_policy(POP)
